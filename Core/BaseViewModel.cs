﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutoNPC;
using FluentValidation;
using FluentValidation.Attributes;
using PostSharp;

namespace Core
{
    [AutoNotifyPropertyChangedAttribute]
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string prop)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(prop));
        }

        private static bool IsIDataErrorInfo(Type type)
        {
            if (typeof(IDataErrorInfo).IsAssignableFrom(type) && !type.IsAbstract && !type.IsInterface)
                return type.GetConstructor(Type.EmptyTypes) != null;
            return false;
        }

        /// <summary>
        /// Returns all the validation errors for properties of the ViewModel.
        /// It will return one line per property.
        /// </summary>
        public IEnumerable<string> ValidationErrors
        {
            get
            {
                var props = GetType()
                    .GetProperties(BindingFlags.Public | BindingFlags.DeclaredOnly | BindingFlags.Instance)
                    .Where(x => IsIDataErrorInfo(x.PropertyType))
                    .Select(x => (IDataErrorInfo)x.GetValue(this));
                return props.Select(x => x.Error);
            }
        }
    }
}
