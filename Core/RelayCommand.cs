﻿using System;
using System.Windows.Input;

namespace Core
{
    public class RelayCommand : ICommand
    {
        private readonly Action<object> _execute;
        private readonly Predicate<object> _predicate;

        public RelayCommand(Action<object> execute, Predicate<object> predicate = null)
        {
            _execute = execute;
            _predicate = predicate;
        }

        public void Execute(object parameter)
        {
            _execute(parameter);
        }

        public bool CanExecute(object parameter)
        {
            return _predicate == null || _predicate(parameter);
        }


        public event System.EventHandler CanExecuteChanged;

        protected virtual void OnCanExecuteChanged()
        {
            var handler = CanExecuteChanged;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
    }
}