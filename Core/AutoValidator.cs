﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using PostSharp.Aspects;
using PostSharp.Aspects.Advices;

namespace Core
{
    [Serializable]
    public sealed class AutoValidator : CompositionAspect, IInstanceScopedAspect, IDataErrorInfo
    {
        private class DataErrorInfoImpl<TModel> : IDataErrorInfo where TModel : class
        {
            public DataErrorInfoImpl(TModel model, IValidator<TModel> validator)
            {
                Model = model;
                Validator = validator;
            }

            private TModel Model
            {
                get;
                set;
            }

            /// <summary>
            /// The validator used to validate this object.
            /// This is typically injected by an inversion of dependence container.
            /// </summary>
            private IValidator<TModel> Validator
            {
                get;
                set;
            }

            /// <summary>
            /// See IDataErrorInfo.Error
            /// </summary>
            string IDataErrorInfo.Error
            {
                get
                {
                    return Validator != null
                                        ? string.Join(Environment.NewLine, Validator.Validate(Model).Errors.Select(x => x.ErrorMessage).ToArray())
                                        : string.Empty;
                }
            }

            /// <summary>
            /// See IDataErrorInfo.this[]
            /// </summary>
            /// <param name="propertyName">The property to get the error for</param>
            /// <returns>An empty string if no error, the error otherwise</returns>
            string IDataErrorInfo.this[string propertyName]
            {
                get
                {
                    if (Validator != null)
                    {
                        var results = Validator.Validate<TModel>(Model, propertyName);
                        if (results != null && results.Errors.Any())
                        {
                            var errors = string.Join(Environment.NewLine, results.Errors.Select(x => x.ErrorMessage).ToArray());
                            return errors;
                        }
                    }
                    return string.Empty;
                }
            }
        }

        private IDataErrorInfo _impl;
        private readonly Type _validationType;

        public AutoValidator(Type validationType)
        {
            _validationType = validationType;
        }

        protected override Type[] GetPublicInterfaces(Type targetType)
        {
            return new[] { typeof(IDataErrorInfo) };
        }

        public override object CreateImplementationObject(AdviceArgs args)
        {
            var instanceType = args.Instance.GetType();
            var concreteType = typeof(DataErrorInfoImpl<>).MakeGenericType(new Type[] { instanceType });
            _impl = (IDataErrorInfo)Activator.CreateInstance(concreteType, args.Instance, Activator.CreateInstance(_validationType));
            return _impl;
        }

        public object CreateInstance(AdviceArgs adviceArgs)
        {
            return MemberwiseClone();
        }

        public void RuntimeInitializeInstance()
        {

        }

        [IntroduceMember]
        public string Error
        {
            get
            {
                return _impl.Error;
            }
        }

        [IntroduceMember]
        public string this[string propertyName]
        {
            get
            {
                return _impl[propertyName];
            }
        }
    }
}
