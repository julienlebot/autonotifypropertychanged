﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Core;
using FluentValidation;

namespace ViewModels
{
    [AutoValidator(typeof(ProductAValidtor))]
    public class ProductA
    {
        public int Value
        {
            get;
            set;
        }
    }

    public class ModelWrapper<T> : BaseViewModel
    {
        public T Model
        {
            get;
            set;
        }
    }

    public class ProductAViewModel : ModelWrapper<ProductA>
    {
        public bool Selected
        {
            get;
            set;
        }
    }

    [AutoValidator(typeof(ProductBValidator))]
    public class ProductB
    {
        public string Name
        {
            get;
            set;
        }

        public ObservableCollection<ProductA> TheAProducts
        {
            get;
            set;
        }

        public ProductC Answer
        {
            get;
            set;
        }

        private void LoadAProducts()
        {
            TheAProducts.Add(
                             new ProductA
                             {
                                 Value = 34
                             });
            TheAProducts.Add(
                             new ProductA
                             {
                                 Value = 42
                             });
            TheAProducts.Add(
                             new ProductA
                             {
                                 Value = -18
                             });
        }

        public ProductB()
        {
            Name = "Psyko";
            TheAProducts = new ObservableCollection<ProductA>();
            Answer = new ProductC();
            LoadAProducts();
        }
    }

    [AutoValidator(typeof(ProductCValidator))]
    public class ProductC
    {
        public float TheAnswer
        {
            get;
            set;
        }
    }

    public class ProductAValidtor : AbstractValidator<ProductA>
    {
        public ProductAValidtor()
        {
            RuleFor(x => x.Value).Equal(42).WithMessage("La valeur doit être égal à 42");
        }
    }

    public class ProductBValidator : AbstractValidator<ProductB>
    {
        public ProductBValidator()
        {
            RuleFor(x => x.Name).Equal("FooBar").WithMessage("La valeur doit être égal à FooBar");
            RuleFor(x => x.Answer).SetValidator(new ProductCValidator());
            RuleFor(x => x.TheAProducts).SetCollectionValidator(new ProductAValidtor());
        }
    }

    public class ProductCValidator : AbstractValidator<ProductC>
    {
        public ProductCValidator()
        {
            RuleFor(x => x.TheAnswer).GreaterThan(42.0f).WithMessage("La valeur doit être supérieur à la réponse à tout.");
        }
    }

    public class ZgegViewModel : BaseViewModel
    {
        public ZgegViewModel()
        {
            IsEditEnable = false;
            Model = new ProductB();
            TheAProducts = new ObservableCollection<ProductAViewModel>(Model.TheAProducts.Select(x => new ProductAViewModel{ Model = x }));
        }

        public ProductB Model
        {
            get;
            set;
        }

        public bool IsEditEnable
        {
            get;
            set;
        }

        public ObservableCollection<ProductAViewModel> TheAProducts
        {
            get;
            set;
        }

        public ProductA SelectedProduct
        {
            get;
            set;
        }

        public bool CheckAll
        {
            get;
            set;
        }

        private ICommand _doCheckAll = null;
        public ICommand DoCheckAll
        {
            get
            {
                if (null == _doCheckAll)
                {
                    _doCheckAll = new RelayCommand(
                        (o) =>
                        {
                            foreach (var pa in TheAProducts)
                            {
                                pa.Selected = CheckAll;
                            }
                        });
                }
                return _doCheckAll;
            }
        }

        private void AddProfile()
        {
            IsEditEnable = true;
        }

        private ICommand _addProfileCmd = null;
        public ICommand AddProfileCommand
        {
            get
            {
                if (_addProfileCmd == null)
                    _addProfileCmd = new RelayCommand((o) => AddProfile());
                return _addProfileCmd;
            }
        }
    }
}
