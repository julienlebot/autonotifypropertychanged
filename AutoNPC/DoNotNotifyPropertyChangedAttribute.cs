﻿using System;

namespace AutoNPC
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class DoNotNotifyPropertyChangedAttribute : Attribute
    {
    }
}