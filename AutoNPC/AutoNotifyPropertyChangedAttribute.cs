using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using PostSharp.Aspects;
using PostSharp.Extensibility;

namespace AutoNPC
{
    [MulticastAttributeUsage(MulticastTargets.Class, Inheritance = MulticastInheritance.Strict)]
    [Serializable]
    public sealed class AutoNotifyPropertyChangedAttribute : TypeLevelAspect, IAspectProvider
    {
        // This method is called at build time and should just provide other aspects.  
        public IEnumerable<AspectInstance> ProvideAspects(object targetElement)
        {
            var targetType = (Type)targetElement;

            // Add a NotifyPropertyChanged attribute to every relevant property. 
            return targetType.GetProperties(BindingFlags.Public | BindingFlags.DeclaredOnly | BindingFlags.Instance)
                             .Where(property => property.CanWrite && !property.IsDefined(typeof(DoNotNotifyPropertyChangedAttribute), false))
                             .Select(property => new AspectInstance(property, new NotifyPropertyChangedAspect()));
        }
    }
}