﻿using System;
using PostSharp.Aspects;
using PostSharp.Aspects.Advices;

namespace AutoNPC
{
    [Serializable]
    public sealed class NotifyPropertyChangedAspect : LocationInterceptionAspect, IInstanceScopedAspect
    {
        [ImportMember("OnPropertyChanged", IsRequired = true)]
        public Action<string> OnPropertyChanged;

        public override void OnSetValue(LocationInterceptionArgs args)
        {
            base.OnSetValue(args);
            OnPropertyChanged(args.LocationName);
        }

        public object CreateInstance(AdviceArgs adviceArgs)
        {
            return MemberwiseClone();
        }

        public void RuntimeInitializeInstance()
        {

        }
    }
}