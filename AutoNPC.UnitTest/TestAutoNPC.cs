﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Core;
using NUnit.Framework;
using PostSharp;
using Rhino.Mocks;
using ViewModels;
using RelayCommand = Core.RelayCommand;

namespace AutoNPC.UnitTest
{
    [TestFixture]
    // ReSharper disable once InconsistentNaming
    public class TestAutoNPC
    {
        [Test]
        public void Should_ClassWithAspectImplementNotifyPropertyChanged()
        {
            Assert.DoesNotThrow(() => Post.Cast<BaseViewModel, INotifyPropertyChanged>(new TestClass()));
            Assert.DoesNotThrow(() => Post.Cast<BaseViewModel, INotifyPropertyChanged>(new OtherTestClass()));
        }

        [Test]
        public void Should_ClassWithAspectRaiseNotifyPropertyChanged()
        {
            var test = new TestClass();
            var handler = MockRepository.GenerateStub<PropertyChangedEventHandler>();
            string propertyString = null;
            handler.Stub(act => act(Arg<object>.Is.Anything, Arg<PropertyChangedEventArgs>.Is.Anything))
                               .WhenCalled((a) => propertyString = ((PropertyChangedEventArgs)a.Arguments[1]).PropertyName);
            test.PropertyChanged += handler;
            test.TestString = "test";
            Assert.IsNotNullOrEmpty(propertyString);
            Assert.IsTrue(propertyString == "TestString");
        }

        [Test]
        public void Should_ClassWithAspectNotRaiseNotifyPropertyChangedOnIgnoredMembers()
        {
            var test = new TestClass();
            var handler = MockRepository.GenerateStub<PropertyChangedEventHandler>();
            handler.Stub(act => act(Arg<object>.Is.Anything, Arg<PropertyChangedEventArgs>.Is.Anything));
            test.PropertyChanged += handler;
            test.DoubleTest = 3;
            handler.AssertWasNotCalled(act => act(Arg<object>.Is.Anything, Arg<PropertyChangedEventArgs>.Is.Anything));
        }

        [Test]
        public void Should_BaseViewModelHasValidationErrors()
        {
            var vm = new ZgegViewModel();
            var vErrs = vm.ValidationErrors;
            Assert.IsNotEmpty(vErrs);
        }
    }

    public class TestClass : BaseViewModel
    {
        public string TestString
        {
            get;
            set;
        }

        [DoNotNotifyPropertyChanged]
        public int DoubleTest
        {
            get;
            set;
        }
    }

    public class OtherTestClass : BaseViewModel
    {
        public TestClass TestClass2
        {
            get;
            set;
        }

        public string SpecialMember
        {
            get;
            private set;
        }

        private ICommand _cmdWithDel = null;

        // The official PostSharp version of NotifyPropertyChanged does not like properties
        // that call delegate inside of them.
        public ICommand CommandWithDelegate
        {
            get
            {
                if (_cmdWithDel == null)
                    _cmdWithDel = new RelayCommand((o) => SpecialMember = "Sekrit", (o) => o != null);
                return _cmdWithDel;
            }
            set
            {
                _cmdWithDel = value;
            }
        }
    }
}
